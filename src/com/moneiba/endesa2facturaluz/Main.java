package com.moneiba.endesa2facturaluz;

import com.moneiba.endesa2facturaluz.configuration.Configuration;
import com.moneiba.endesa2facturaluz.utils.Files;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Request;
import spark.Response;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

public class Main {

  private static final Logger logger = LogManager.getLogger(Main.class);
  private static String appFullName = Configuration.appCaption() + " v" + Configuration.version();

  public static void main(String[] args) throws IOException {
    System.out.println(appFullName);
    logger.info(appFullName);
    logger.info("Start application.");

    if ((args.length == 0) || (args.length > 2)) {
      System.out.println("Uso: java -jar " + Configuration.appName() + " <endesa csv file> <out file>");
    } else {
      if (args.length == 1) {
        int appPort = Integer.valueOf(args[0]);

        System.out.println("Port: " + appPort);
        port(appPort);

        get("/", (req, res) -> getWeb());
        post("/", Main::postWeb);

      } else {
        Convert(args[0], args[1]);
        System.out.println("Fichero '"+args[1]+"' generado correctamente.");
      }
    }
  }

  private static String getWeb() {
    return "<html>" +
           "  <head>" +
           "    <title>"+Configuration.appCaption()+"</title>" +
           "  </head>" +
           "  <body>" +
           "    <h1>"+appFullName+"</h1>" +
           "    <form method='post' enctype='multipart/form-data'>\n" +
           "      <input type='file' name='uploaded_file'>\n" +
           "      <button>Enviar el fichero CSV</button>\n" +
           "    </form>" +
           "  </body>" +
           "</html>";
  }

  private static HttpServletResponse postWeb(Request req, Response res) throws IOException, ServletException {
    req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/tmp"));
      try {
        InputStream is = req.raw().getPart("uploaded_file").getInputStream();
        String csv = IOUtils.toString(is, "UTF-8");
        List<String> content = new ArrayList<>(Arrays.asList(csv.split("\n")));


        res.raw().setContentType("application/octet-stream");
        res.raw().setHeader("Content-Disposition","attachment; filename='consumo.csv'");

        HttpServletResponse raw = res.raw();
        PrintWriter out = raw.getWriter();

        out.print(Convert(content));
        out.flush();

      } catch (Exception e) {
        e.printStackTrace();
      }

      return res.raw();
  }

  private static String Convert(List<String> content) throws IOException {
    content = cleanContent(content);
    String result = "CUPS;Fecha;Hora;Consumo_kWh;Metodo_obtencion\n";

    String cups = content.get(0).substring(content.get(0).indexOf(", ") + 2).replaceAll("\n", "").replaceAll("\r","");

    for (int x = 6; x < content.size(); x++) {
      String[] line = content.get(x).split(",");
      if (line.length > 3) {

        String[] aDate = line[0].replaceAll(" ", "").split("-");
        String sDate = aDate[2] + "/" + aDate[1] + "/" + aDate[0];
        float consumption = Float.parseFloat(line[2].replaceAll(" ", "")) / 1000;

        result += cups + ";" + sDate + ";" + line[1].replaceAll(" ", "") + ";" + Float.toString(consumption) + ";R\n";
        System.out.println(x);
      }
    }
    return result;
  }

  private static void Convert(String fileIn, String fileOut) throws IOException {
    new Files().saveTextFile(fileOut, Convert(Files.readFile(fileIn)), true);
  }

  private static List<String>  cleanContent(List<String> content) {
    List<String> result = new ArrayList<>();

    for(String item: content) {
      if (! "\n".equals(item) && ! "\r".equals(item) && ! "\r\n".equals(item) && ! "".equals(item))
        result.add(item);
    }
    return result;
  }
}
