package com.moneiba.endesa2facturaluz.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {

  public static String version() {
    String version = "";
    try {
      version = getInfo("version");
    } catch (IOException e) {
      System.err.println("I can't get version number.");
    }
    return version;
  }

  public static String appName() {
    String appName = "";
    try {
      appName = getInfo("artifactId");
    } catch (IOException e) {
      System.err.println("I can't get application name.");
    }
    return appName;
  }

  public static String appCaption() {
    String appCaption = "";
    try {
      appCaption = getInfo("name");
    } catch (IOException e) {
      System.err.println("I can't get application caption.");
    }
    return appCaption;
  }

  private static String getInfo(String parameter) throws IOException {
    InputStream resourceAsStream = Configuration.class.getResourceAsStream("/version.properties");
    Properties prop = new Properties();
    prop.load( resourceAsStream );
    return prop.getProperty(parameter);
  }
}
