package com.moneiba.endesa2facturaluz.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Files {

  private Logger logger;

  public Files() {
    logger = LogManager.getLogger(this.getClass());
  }

  public static List<String> readFile(String fileName) throws IOException {
    try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
      String line = br.readLine();
      List<String> result = new ArrayList<>();

      while (line != null) {
        result.add(line);
        line = br.readLine();
      }
      return result;
    }
  }

  private static Boolean fileExists(String fileName) {
    File file = new File(fileName);
    return file.exists();
  }

  private static String[] list(String dir) {
    File directory = new File(dir);
    File[] files = directory.listFiles();
    ArrayList<String> fileList = new ArrayList<>();

    if (files != null) {
      for (File file : files) {

        if (file.isDirectory()) {
          String[] list = list(file.getAbsolutePath());

          Collections.addAll(fileList, list);
        } else {
          fileList.add(file.getAbsoluteFile().toString());
        }
      }
    }

    String[] result = new String[fileList.size()];
    for (int x = 0; x < result.length; x++) {
      result[x] = fileList.get(x);
    }

    return result;
  }

  public void makeDir(String dir) {
    logger.info("Making dir: " + dir);
    File directory = new File(dir);

    if (!directory.exists())
      if (!directory.mkdirs()) {
        logger.error("I can't create directory '" + dir + "'.");
      }
  }

  public void saveTextFile(String fileName, String text, Boolean overwrite) throws IOException {
    if (overwrite) new Files().remove(fileName);
    saveTextFile(fileName, text);
  }

  public void saveTextFile(String fileName, String text) throws IOException {
    if (fileExists(fileName))
      throw new IOException("The file exists.");

    try {
      String parentDir = new File(fileName).getParent();
      if ((parentDir != null) && ! fileExists(parentDir))
        makeDir(parentDir);

      FileWriter writer = new FileWriter(fileName);
      writer.write(text);
      writer.close();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }

  public void remove(String fileName) {
    logger.info("Delete file: " + fileName);
    if (!fileExists(fileName))
      logger.warn("Delete file: " + fileName + ". File not exists.");
    else {
      File file = new File(fileName);
      try {
        boolean success = file.delete();
        if (!success)
          throw new IllegalArgumentException("Delete: deletion failed.");
      } catch (Exception e) {
        String message = e.getMessage() + " File: " + fileName;
        logger.error(message);
        System.out.println(message);
      }
    }
  }

  public void removeDir(String dir) {
    if (fileExists(dir)) {
      File directory = new File(dir);
      File[] files = directory.listFiles();

      assert files != null;
      for (File file1 : files) {

        if (file1.isDirectory()) {
          this.removeDir(dir + "/" + file1.getName());
        }

        File file = new File(dir + "/" + file1.getName());
        if ((file.exists()) && (!file.isDirectory()) && (!file.delete())) {
          logger.error("I can't delete file '" + dir + "/" + file1.getName() + "'.");
        }
      }
      if (!directory.delete()) {
        logger.error("I can't delete directory '" + dir + "'.");
      }
    } else {
      logger.info("Removing dir: Directory '" + dir + "' not exists.");
    }
  }

  public static String getCurrentDir() {
    return System.getProperty("user.dir");
  }
}
